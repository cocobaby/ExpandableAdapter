# ExpandableAdapter

--- 

##预览

![演示](http://git.oschina.net/cocobaby/ExpandableAdapter/raw/master/ExpandableAdapter_showcase.gif)

##描述


可展开的列表适配器，常用于隐藏菜单之类的UI交互设计中


##使用说明

- 自定义一个adapter，继承 **ExpandableAdapter**，类似如下：

```java
public class MyAdapter extends ExpandableAdapter<String> {

    public MyAdapter(Context context, int layoutRes, int aboveRes, int expandRes) {
        super(context, layoutRes, aboveRes, expandRes);
    }

    @Override
    protected View getAboveView(int position, View convertView, ViewGroup parent) {
        ViewHolerAbove viewHolerAbove = null;
        if (convertView == null) {
            viewHolerAbove = new ViewHolerAbove();
            convertView = mInflater.inflate(R.layout.item_ab, parent, false);
            viewHolerAbove.textView = (TextView) convertView
                    .findViewById(R.id.textView1);
            convertView.setTag(viewHolerAbove);
        } else {
            viewHolerAbove = (ViewHolerAbove) convertView.getTag();
        }
        viewHolerAbove.textView.setText(getItem(position));
        return convertView;
    }

    @Override
    protected View getExpandView(int position, View convertView,
            ViewGroup parent) {
        ViewHolderExpand viewHolderExpand = null;
        if (convertView == null) {
            viewHolderExpand = new ViewHolderExpand();
            convertView = mInflater.inflate(R.layout.item_ex, parent, false);
            viewHolderExpand.textView = (TextView) convertView
                    .findViewById(R.id.textView1);
            convertView.setTag(viewHolderExpand);
        } else {
            viewHolderExpand = (ViewHolderExpand) convertView.getTag();
        }
        viewHolderExpand.textView.setText(getItem(position));
        return convertView;
    }

    public static class ViewHolerAbove {
        TextView textView;
    }

    public static class ViewHolderExpand {
        TextView textView;
    }

}
```

- 之后和普通的列表适配器同样的使用




```java
listView.setAdapter(mAdapter);
```

##鸣谢

- [nineoldandroids](http://nineoldandroids.com/)

##更多

- [我的个人博客](http://www.hikyson.cn)

- [我的开源项目](http://git.oschina.net/cocobaby)

- [我的新浪微博](http://weibo.com/1980495343/profile?rightmod=1&wvr=6&mod=personinfo)

##License

Copyright (c) 2014 Kyson

Licensed under the GPL V2
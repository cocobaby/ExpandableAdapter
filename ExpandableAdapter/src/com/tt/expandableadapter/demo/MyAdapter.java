package com.tt.expandableadapter.demo;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tt.expandableadapter.ExpandableAdapter;
import com.tt.expandableadapter.R;
import com.tt.expandableadapter.R.id;
import com.tt.expandableadapter.R.layout;

public class MyAdapter extends ExpandableAdapter<String> {

    public MyAdapter(Context context, int layoutRes, int aboveRes, int expandRes) {
        super(context, layoutRes, aboveRes, expandRes);
    }

    @Override
    protected View getAboveView(int position, View convertView, ViewGroup parent) {
        ViewHolerAbove viewHolerAbove = null;
        if (convertView == null) {
            viewHolerAbove = new ViewHolerAbove();
            convertView = mInflater.inflate(R.layout.item_ab, parent, false);
            viewHolerAbove.textView = (TextView) convertView
                    .findViewById(R.id.textView1);
            convertView.setTag(viewHolerAbove);
        } else {
            viewHolerAbove = (ViewHolerAbove) convertView.getTag();
        }
        viewHolerAbove.textView.setText(getItem(position));
        return convertView;
    }

    @Override
    protected View getExpandView(int position, View convertView,
            ViewGroup parent) {
        ViewHolderExpand viewHolderExpand = null;
        if (convertView == null) {
            viewHolderExpand = new ViewHolderExpand();
            convertView = mInflater.inflate(R.layout.item_ex, parent, false);
            viewHolderExpand.textView = (TextView) convertView
                    .findViewById(R.id.textView1);
            convertView.setTag(viewHolderExpand);
        } else {
            viewHolderExpand = (ViewHolderExpand) convertView.getTag();
        }
        viewHolderExpand.textView.setText(getItem(position));
        return convertView;
    }

    public static class ViewHolerAbove {
        TextView textView;
    }

    public static class ViewHolderExpand {
        TextView textView;
    }

}

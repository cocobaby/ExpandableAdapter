package com.tt.expandableadapter.demo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ListView;

import com.tt.expandableadapter.R;
import com.tt.expandableadapter.R.id;
import com.tt.expandableadapter.R.layout;

public class MainActivity extends Activity {
    private ListView mListView;

    private MyAdapter mAdapter;

    protected void onDestroy() {
        super.onDestroy();
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) this.findViewById(R.id.list);
        mAdapter = new MyAdapter(MainActivity.this, R.layout.item, R.id.item_1,
                R.id.item_2);
        mListView.setAdapter(mAdapter);

        List<String> strings = new ArrayList<String>();
        for (int i = 0; i < 50; i++) {
            strings.add("this is item : " + i);
        }
        mAdapter.setList(strings);
        mAdapter.notifyDataSetChanged();
    }
}
